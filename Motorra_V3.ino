#include <AFMotor.h>

AF_DCMotor Motorra(4);                          //Aukera 4 motorretako bat. Kasu honetan M4 aukeratu da.
int balioa=0;

void setup() {
  Serial.begin(9600);
  Serial.println("Adafruit Motorshield v1 - DC Motor Diseinua V.1");
  Motorra.setSpeed(130);                        //Motorarren abiadura definituko dugu.
  Motorra.run(RELEASE);                         //Motorra gelditu
  pinMode(13, OUTPUT);                          //13. pin-a irteera deklaratu. Bertara Led gorria lotu.
  pinMode(11, OUTPUT);                          //11. pin-a irteera deklaratu. Bertara Led berdea lotu.
}

void loop() {
     balioa=analogRead(0);                      //Analog0 pin-a irakurri eta joystickaren bere balioa hartu.
     Serial.print("Joysticka Irakurri=  ");     //Monitor Seriean "Joyistika irakurri" idatzi
     Serial.println(balioa);                    //Monitor Seriean uneoroko balioa jarri

     if (balioa<412){                           //Joystickaren balioa 412 edo txikiago denean...
        balioa=map(balioa,412,0,130,255);                       //Joystiren balioa eta abiaduraren arteko harremana egin.
        Motorra.setSpeed(balioa);                               //Mapeatutako abiadurara ibili
        Motorra.run(FORWARD);                                   //Motorra ezkerretara biraka jarri
        Serial.println("<412 EZKERRETARA, ARGI BERDEA ON");     //Monitor Seriean idatzi
        digitalWrite(11, HIGH);                                 //Led berdea piztu
        digitalWrite(13, LOW);                                  //Led gorria itzali
     }

     else if (balioa>612){                      //Joystickaren balioa 612 edo handiago denean...
          balioa=map(balioa,612,1023,130,255);                  //Joystiren balioa eta abiaduraren arteko harremana egin.
          Motorra.setSpeed(balioa);                             //Mapeatutako abiadurara ibili
          Motorra.run(BACKWARD);                                //Motorra eskubira biraka jarri
          Serial.println(">612 ESKUBITARA, ARGI BERDEA ON");    //Monitor Seriean idatzi
          digitalWrite(11, HIGH);                               //Led berdea piztu
          digitalWrite(13, LOW);                                //Led gorria itzali
          }

          else {                                //Joystickaren balioa gainontzekoa denean... 
              Motorra.run(RELEASE);                             //Motorra gelditu
              Serial.println("ERDIAN GELDIK, ARGI GORRIA ON");  //Monitor Seriean idatzi
              digitalWrite(13, HIGH);                           //Led gorria piztu
              digitalWrite(11, LOW);                            //Led berdea itzali
              }   
     delay(0);             
}